package com.sc.social;

import lombok.Data;
import org.springframework.social.security.SocialAuthenticationFilter;
import org.springframework.social.security.SpringSocialConfigurer;

/**
 * @author sc
 * Created on  2018/1/18
 */
@Data
public class MySpringSocialConfig extends SpringSocialConfigurer {

    private String filterProcess;

    private SocialAuthFilterPostProcessor socialAuthFilterPostProcessor;

    public MySpringSocialConfig(String filterProcess) {
        this.filterProcess = filterProcess;
    }

    @Override
    protected <T> T postProcess(T object) {
        SocialAuthenticationFilter filter = (SocialAuthenticationFilter)super.postProcess(object);
        filter.setFilterProcessesUrl(filterProcess);
        if(socialAuthFilterPostProcessor!=null){
            socialAuthFilterPostProcessor.process(filter);
        }
        return (T) filter;
    }
}
