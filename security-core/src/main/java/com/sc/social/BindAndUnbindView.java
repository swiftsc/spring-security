package com.sc.social;

import org.springframework.web.servlet.view.AbstractView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;

/**
 * @author sc
 * Created on  2018/1/18
 */
public class BindAndUnbindView extends AbstractView{


    @Override
    protected void renderMergedOutputModel(Map<String, Object> model, HttpServletRequest request, HttpServletResponse response) throws Exception {
        response.setContentType("text/html;charset=utf-8");
        if(model.get("connection")==null){
            response.getWriter().write("<h1>解绑成功</h1>");
        }
        response.getWriter().write("<h1>绑定成功</h1>");
    }
}
