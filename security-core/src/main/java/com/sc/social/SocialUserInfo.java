package com.sc.social;

import lombok.Data;

/**
 * @author sc
 * Created on  2018/1/18
 */
@Data
public class SocialUserInfo {
    private String providerId;
    private String providerUserId;
    private String nackName;
    private String image;
}
