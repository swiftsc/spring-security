package com.sc.social.qq.api;

import java.io.IOException;

/**
 * @author sc
 * Created on  2018/1/18
 */
public interface QQ {
    QQUserInfo getUserInfo();
}
