package com.sc.social.qq;

import lombok.Data;
import org.springframework.boot.autoconfigure.social.SocialProperties;

/**
 * @author sc
 * Created on  2018/1/18
 */
@Data
public class QQProperties extends SocialProperties{
    private String providerId = "qq";
}
