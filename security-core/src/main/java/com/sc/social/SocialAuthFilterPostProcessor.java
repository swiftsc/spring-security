package com.sc.social;

import org.springframework.social.security.SocialAuthenticationFilter;

/**
 * @author sc
 * Created on  2018/1/24
 */

public interface SocialAuthFilterPostProcessor {
    void process(SocialAuthenticationFilter socialAuthenticationFilter);
}
