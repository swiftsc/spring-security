package com.sc.social.wechet;

import lombok.Data;
import org.springframework.boot.autoconfigure.social.SocialProperties;

/**
 * @author sc
 * Created on  2018/1/18
 */
@Data
public class WxProperties extends SocialProperties{
    private String providerId = "weixin";
}
