package com.sc.social.wechet.connect;

import lombok.Data;
import org.springframework.social.oauth2.AccessGrant;

/**
 * @author sc
 * Created on  2018/1/18
 */
@Data
public class WechetAccessGrant extends AccessGrant {

    private String openId;


    public WechetAccessGrant() {
        super("");
    }

    public WechetAccessGrant(String accessToken, String scope, String refreshToken, Long expiresIn) {
        super(accessToken, scope, refreshToken, expiresIn);
    }
}
