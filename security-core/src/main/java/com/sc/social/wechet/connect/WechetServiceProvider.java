package com.sc.social.wechet.connect;

import com.sc.social.wechet.api.Wechet;
import com.sc.social.wechet.api.WechetImpl;
import org.springframework.social.oauth2.AbstractOAuth2ServiceProvider;
import org.springframework.social.oauth2.OAuth2Operations;

/**
 * @author sc
 * Created on  2018/1/18
 */
public class WechetServiceProvider extends AbstractOAuth2ServiceProvider<Wechet> {

    /**
     * 微信获取授权码的url
     */
    private static final String URL_AUTHORIZE = "https://open.weixin.qq.com/connect/qrconnect";
    /**
     * 微信获取accessToken的url
     */
    private static final String URL_ACCESS_TOKEN = "https://api.weixin.qq.com/sns/oauth2/access_token";



    public WechetServiceProvider(String appId, String appSecret) {
        super(new WechetOAuthTemplate(appId, appSecret,URL_AUTHORIZE,URL_ACCESS_TOKEN));
    }

    @Override
    public Wechet getApi(String s) {
        return new WechetImpl(s);
    }
}
