package com.sc.social.wechet.config;

import com.sc.security.properties.SecurityProperties;
import com.sc.social.BindAndUnbindView;
import com.sc.social.wechet.WxProperties;
import com.sc.social.wechet.connect.WechetConnectionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.autoconfigure.social.SocialAutoConfigurerAdapter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.social.connect.ConnectionFactory;
import org.springframework.web.servlet.View;

/**
 * @author sc
 * Created on  2018/1/18
 */
@Configuration
@ConditionalOnProperty(prefix = "sc.security.social.weixin",name = "app-id")
public class WechetAutoConfig extends SocialAutoConfigurerAdapter{

    @Autowired
    private SecurityProperties securityProperties;

    @Override
    protected ConnectionFactory<?> createConnectionFactory() {
        WxProperties wxProperties = securityProperties.getSocial().getWeixin();
        return new WechetConnectionFactory(wxProperties.getProviderId(),wxProperties.getAppId(),wxProperties.getAppSecret());
 }

    @Bean({"connect/weixinConnected","connect/weixinConnect"})
    @ConditionalOnMissingBean(name = "weixinConnectedView")
    public View bindAndUnbind(){
        return new BindAndUnbindView();
    }

}
