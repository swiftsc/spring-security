package com.sc.social.wechet.api;

/**
 * @author sc
 * Created on  2018/1/18
 */
public interface Wechet {
    WechetUserInfo getUserInfo(String openId);
}
