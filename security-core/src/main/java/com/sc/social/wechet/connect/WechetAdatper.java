package com.sc.social.wechet.connect;

import com.sc.social.wechet.api.Wechet;
import com.sc.social.wechet.api.WechetUserInfo;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.springframework.social.connect.ApiAdapter;
import org.springframework.social.connect.ConnectionValues;
import org.springframework.social.connect.UserProfile;

/**
 * @author sc
 * Created on  2018/1/18
 */

public class WechetAdatper implements ApiAdapter<Wechet> {

    private String openId;

    public WechetAdatper(String openId) {
        this.openId = openId;
    }
    public WechetAdatper(){}

    @Override
    public boolean test(Wechet wechet) {
        return true;
    }

    @Override
    public void setConnectionValues(Wechet wechet, ConnectionValues values) {
        WechetUserInfo userInfo = wechet.getUserInfo(openId);
        values.setProviderUserId(userInfo.getOpenid());
        values.setImageUrl(userInfo.getHeadimgurl());
        values.setDisplayName(userInfo.getNickname());

    }

    @Override
    public UserProfile fetchUserProfile(Wechet wechet) {
        return null;
    }

    @Override
    public void updateStatus(Wechet wechet, String s) {

    }
}
