package com.sc.security.properties;

import lombok.Data;

/**
 * @author sc
 * Created on  2018/1/17
 * 图形验证码配置
 */
@Data
public class ImageCodeProperties extends SmsCodeProperties{
    public ImageCodeProperties(){
        setLength(4);
    }
    private int width = 67;
    private int height = 23;
}
