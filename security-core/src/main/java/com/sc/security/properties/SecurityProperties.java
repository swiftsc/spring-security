package com.sc.security.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author sc
 * Created on  2018/1/16
 */
@ConfigurationProperties(prefix = "sc.security")
@Data
public class SecurityProperties {
    /**
     * 这里要和配置文件中的名称对应好，是通过映射来绑定数据的
     */
    private BrowserProperties browser = new BrowserProperties();
    private ValidateCodeProperties code  = new ValidateCodeProperties();
    private SocialProperties social = new SocialProperties();
    private SessionProperties session = new SessionProperties();
}
