package com.sc.security.properties;

import com.sc.social.qq.QQProperties;
import com.sc.social.wechet.WxProperties;
import lombok.Data;

/**
 * @author sc
 * Created on  2018/1/18
 */
@Data
public class SocialProperties {

    private String filterProcessUrl="/auth";
    private QQProperties qq = new QQProperties();
    private WxProperties weixin = new WxProperties();
}
