package com.sc.security.properties;

import lombok.Data;

/**
 * @author sc
 * Created on  2018/1/17
 * 图形验证码配置
 */
@Data
public class SmsCodeProperties {
    private int length = 6;
    private int expire = 60*3;
    private String url ;
}
