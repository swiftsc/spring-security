package com.sc.security.properties;

import lombok.Data;

/**
 * @author sc
 * Created on  2018/1/17
 */
@Data
public class ValidateCodeProperties {
    private ImageCodeProperties image = new ImageCodeProperties();
    private SmsCodeProperties sms = new SmsCodeProperties();
}
