package com.sc.security.properties;

import lombok.Data;

/**
 * @author sc
 * Created on  2018/1/16
 */
@Data
public class BrowserProperties {
    private String signUpUrl = SecurityConstants.DEFAULT_SIGNUP_PAGE_URL;
    private String loginPage=SecurityConstants.DEFAULT_LOGIN_PAGE_URL;
    private LoginType loginType = LoginType.JSON;
    private int rememberMeExpire = 60*60*24*7;
    private String logOutSuccessUrl;
    private String logOutUrl=SecurityConstants.DEFAULT_LOGOUT_URL;
}
