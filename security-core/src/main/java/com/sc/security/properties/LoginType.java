package com.sc.security.properties;

/**
 * @author sc
 * Created on  2018/1/16
 */
public enum LoginType {
    REDIRECT,
    JSON
}
