package com.sc.security.properties;

import lombok.Data;

/**
 * @author sc
 * Created on  2018/1/19
 */
@Data
public class SessionProperties {
    private int     maxSessions             =    1;
    private boolean maxSessionPreventLogin  =    false;
    private String  sessionUrl              =    SecurityConstants.DEFAULT_SESSION_INVALID_URL;
}
