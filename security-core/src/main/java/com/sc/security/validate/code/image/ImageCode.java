package com.sc.security.validate.code.image;

import com.sc.security.validate.code.sms.ValidateCode;
import lombok.Data;

import java.awt.image.BufferedImage;

/**
 * @author sc
 * Created on  2018/1/17
 */
@Data
public class ImageCode extends ValidateCode {

    private BufferedImage image;

    public ImageCode(BufferedImage image,String code,int expireIn){
        super(code,expireIn);
        this.image = image;
    }

}
