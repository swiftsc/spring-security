package com.sc.security.validate.code;


import org.springframework.security.core.AuthenticationException;

/**
 * @author sc
 * Created on  2018/1/17
 */
public class ValidateCodeException extends AuthenticationException {
    public ValidateCodeException(String msg) {
        super(msg);
    }
}
