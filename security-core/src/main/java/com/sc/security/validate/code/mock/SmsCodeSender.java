package com.sc.security.validate.code.mock;

/**
 * @author sc
 * Created on  2018/1/17
 */
public interface SmsCodeSender {
    void send(String mobile,String code);
}
