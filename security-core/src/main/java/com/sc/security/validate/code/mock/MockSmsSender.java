package com.sc.security.validate.code.mock;

/**
 * @author sc
 * Created on  2018/1/17
 */
public class MockSmsSender implements SmsCodeSender {
    @Override
    public void send(String mobile, String code) {
        System.out.println("向"+mobile+"发送验证码"+code);
    }
}
