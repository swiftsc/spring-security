package com.sc.security.validate.code.sms;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @author sc
 * Created on  2018/1/17
 */
@Data
@AllArgsConstructor
public class ValidateCode implements Serializable{

    private String code;
    private LocalDateTime expire;

    public ValidateCode(String code, int expireIn){
        this.code = code;
        this.expire = LocalDateTime.now().plusSeconds(expireIn);
    }

    public boolean isExpired(){
        if(LocalDateTime.now().isAfter(expire)){
            return true;
        }
        return false;
    }
}
