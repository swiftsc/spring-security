package com.sc.security.service.sms;

import com.sc.security.properties.SecurityProperties;
import com.sc.security.service.ValidateCodeGenerator;
import com.sc.security.validate.code.sms.ValidateCode;
import org.apache.commons.lang.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.ServletWebRequest;

/**
 * @author sc
 * Created on  2018/1/17
 */
@Component
public class SmsCodeGenDefault implements ValidateCodeGenerator{
    @Autowired
    private SecurityProperties securityProperties;

    @Override
    public ValidateCode generate(ServletWebRequest request) {
        String code = RandomStringUtils.randomNumeric(securityProperties.getCode().getSms().getLength());
        return new ValidateCode(code,securityProperties.getCode().getImage().getExpire());
    }
}
