package com.sc.security.service.sms;

import com.sc.security.service.ValidateCodeType;
import com.sc.security.validate.code.sms.ValidateCode;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.ServletWebRequest;

/**
 * @author sc
 * Created on  2018/1/24
 */
public interface ValidateCodeRepo {
    void save(ServletWebRequest request, ValidateCode validate, ValidateCodeType validateCodeType);
    ValidateCode get(ServletWebRequest request,ValidateCodeType codeType);
    void remove(ServletWebRequest request,ValidateCodeType codeType);
}
