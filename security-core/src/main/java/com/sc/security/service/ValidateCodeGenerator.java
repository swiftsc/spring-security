package com.sc.security.service;


import com.sc.security.validate.code.sms.ValidateCode;
import org.springframework.web.context.request.ServletWebRequest;


/**
 * @author sc
 * Created on  2018/1/17
 */
public interface ValidateCodeGenerator {
    ValidateCode generate(ServletWebRequest request);
}
