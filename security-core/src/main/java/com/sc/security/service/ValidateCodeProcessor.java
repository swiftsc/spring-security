package com.sc.security.service;

import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.context.request.ServletWebRequest;

import java.io.IOException;

/**
 * @author sc
 * Created on  2018/1/17
 *
 * 封装不同校验码的处理逻辑
 */
public interface ValidateCodeProcessor {


    /***
     * 创建验证码
     * @param request
     * @throws IOException
     * @throws ServletRequestBindingException
     */
    void create(ServletWebRequest request) throws IOException, ServletRequestBindingException;

    /***
     * 验证
     * @param request
     */
    void validate(ServletWebRequest request);
}
