package com.sc.security.config;

import com.sc.security.properties.SecurityProperties;
import com.sc.security.service.image.ImageCodeGenDefault;
import com.sc.security.service.ValidateCodeGenerator;
import com.sc.security.validate.code.mock.MockSmsSender;
import com.sc.security.validate.code.mock.SmsCodeSender;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author sc
 * Created on  2018/1/17
 */
@Configuration
public class ValidateCodeBeanConfig {
    @Autowired
    private SecurityProperties securityProperties;

    @Bean
    @ConditionalOnMissingBean(ValidateCodeGenerator.class)
    public ValidateCodeGenerator defaultImageCodeGen(){
        ImageCodeGenDefault imageCodeGenDefault = new ImageCodeGenDefault();
        imageCodeGenDefault.setProperties(securityProperties);
        return imageCodeGenDefault;
    }

    @Bean
    @ConditionalOnMissingBean(SmsCodeSender.class)
    public SmsCodeSender smsCodeGen(){
        return new MockSmsSender();
    }
}
