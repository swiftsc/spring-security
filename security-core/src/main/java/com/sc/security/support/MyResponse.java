package com.sc.security.support;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @author sc
 * Created on  2018/1/16
 */
@Data
@AllArgsConstructor
public class MyResponse {
    private Object content;
}
