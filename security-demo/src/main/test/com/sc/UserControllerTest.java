package com.sc;

import com.sun.scenario.effect.impl.sw.sse.SSEBlend_SRC_OUTPeer;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.Date;

/**
 * @author sc
 * Created on  2018/1/15
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class UserControllerTest {

    @Autowired
    private WebApplicationContext wac;

    private MockMvc mockMvc;

    @Before
    public void setUp(){
        mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
    }

    @Test
    public void QuerySuccess(){
        try {
            String contentAsString = mockMvc.perform(MockMvcRequestBuilders.get("/user/list").
                    contentType(MediaType.APPLICATION_JSON_UTF8)).
                    andExpect(MockMvcResultMatchers.status().isOk()).
                    andExpect(MockMvcResultMatchers.jsonPath("$.length()").value(3)).andReturn()
                    .getResponse().getContentAsString();
            System.out.println(contentAsString);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void getInfoSuccess(){
        try {
            String sc = mockMvc.perform(MockMvcRequestBuilders.get("/user/1").
                    contentType(MediaType.APPLICATION_JSON_UTF8)).
                    andExpect(MockMvcResultMatchers.status().isOk()).
                    andExpect(MockMvcResultMatchers.jsonPath("$.username").value("sc")).
                    andReturn().getResponse().getContentAsString();
            System.out.println(sc);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void getInfoFail() throws Exception {
        String contentAsString = mockMvc.perform(MockMvcRequestBuilders.get("/user/a").
                contentType(MediaType.APPLICATION_JSON_UTF8)).
                andExpect(MockMvcResultMatchers.status().is4xxClientError()).andReturn()
                .getResponse().getContentAsString();
        System.out.println(contentAsString);

    }

    @Test
    public void createSuccess(){
        String content = "{\"username\":\"sc\",\"password\":\"\"}";
        try {
            mockMvc.perform(MockMvcRequestBuilders.post("/user").
                    contentType(MediaType.APPLICATION_JSON_UTF8).content(content)).
                    andExpect(MockMvcResultMatchers.status().isOk())
            .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(1L));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void modifySuccess(){
        String content = "{\"id\":\"1\" ,\"username\":\"sc123\",\"password\":\"789\"}";
        try {
            mockMvc.perform(MockMvcRequestBuilders.put("/user/1").
                    contentType(MediaType.APPLICATION_JSON_UTF8).content(content)).
                    andExpect(MockMvcResultMatchers.status().isOk())
                    .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(1L));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
