package com.sc.codegen;

import com.sc.security.service.ValidateCodeGenerator;
import com.sc.security.validate.code.image.ImageCode;
import org.springframework.web.context.request.ServletWebRequest;

/**
 * @author sc
 * Created on  2018/1/17
 *
 * 自定义一个图片验证码生成器,使用时取消注释
 * 增量式开发
 */
//@Component("imageCodeGen")
public class MyValidateCodeGen implements ValidateCodeGenerator {
    @Override
    public ImageCode generate(ServletWebRequest request) {
        System.out.println("my image codegen gen");
        return null;
    }
}
