package com.sc.domain;

import com.fasterxml.jackson.annotation.JsonView;

import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import org.hibernate.validator.constraints.NotBlank;

/**
 * @author sc
 * Created on  2018/1/15
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
public class User {
    @JsonView(UserQueryView.class)
    private Long id;

    @JsonView(UserQueryView.class)
    private String username;

    @JsonView(UserDetailView.class)
    @NotBlank(message = "密码不能为空")
    @ApiModelProperty("密码")
    private String password;


    public interface UserQueryView{};
    public interface UserDetailView extends UserQueryView{};
}
