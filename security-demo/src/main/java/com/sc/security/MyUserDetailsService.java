package com.sc.security;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.social.security.SocialUser;
import org.springframework.social.security.SocialUserDetails;
import org.springframework.social.security.SocialUserDetailsService;
import org.springframework.stereotype.Component;

/**
 * @author sc
 * Created on  2018/1/16
 * 自定义用户认证逻辑
 * 用于用户校验
 *
 * 处理用户逻辑        UserDetailService
 * 处理用户校验逻辑     UserDetails
 * 处理密码加解密       PasswordEncoder
 */
@Slf4j
@Component
public class MyUserDetailsService implements UserDetailsService,SocialUserDetailsService {

    //注入Dao


    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        log.info("登录用户名称"+s);
        //通过数据库查找对应的密码
        String password = passwordEncoder.encode("123");
        //第三个参数是权限 从数据库中获取
        log.info("密码"+password);
        return new User(s,password,true,true,true,true,AuthorityUtils.commaSeparatedStringToAuthorityList("admin,ROLE_USER"));
    }


    @Override
    public SocialUserDetails loadUserByUserId(String userId) throws UsernameNotFoundException {
        log.info("第三方登录 id:"+userId);
        //省略dao操作
        String password = passwordEncoder.encode("123");
        return new SocialUser(userId,password,true,true,true,true,AuthorityUtils.commaSeparatedStringToAuthorityList("admin"));
    }
}
