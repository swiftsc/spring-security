package com.sc.controller;

import com.fasterxml.jackson.annotation.JsonView;
import com.google.common.collect.Lists;
import com.sc.domain.User;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;


/**
 * @author sc
 * Created on  2018/1/15
 */
@RestController
@RequestMapping("/mobile")
@Slf4j
public class MobileController {


    @RequestMapping("/{id:\\d+}")
    @JsonView(User.UserDetailView.class)
    public User getInfo(@PathVariable("id") Long id){
        User user = new User( id,"sc","pass");
        return user;
    }

    @PostMapping
    public User createUser(@ApiParam("用户") @RequestBody @Valid User user, BindingResult result){
        if(result.hasErrors()){
            result.getAllErrors().forEach(error-> System.out.println(error.getDefaultMessage() ));
        }
        System.out.println(user);
        user.setId(1L);
        return user;
    }


}
