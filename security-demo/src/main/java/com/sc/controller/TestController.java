package com.sc.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author sc
 * Created on  2018/1/17
 */
@RestController
public class TestController {

    @GetMapping("/test")
    public String test(){
        return "remember-me";
    }
}
