package com.sc.app.security.utils;

import com.sc.social.MySpringSocialConfig;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;

/**
 * @author sc
 * Created on  2018/1/24
 */
public class SocialConfigBeanPostProcessor implements BeanPostProcessor {
    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
        return bean;
    }

    /***
     * app 环境
     * @param bean
     * @param beanName
     * @return
     * @throws BeansException
     */
    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        if(StringUtils.equals(beanName,"mySocialSecurityConfig")){
            MySpringSocialConfig mySpringSocialConfig = (MySpringSocialConfig) bean;
            mySpringSocialConfig.signupUrl("/social/signUp");
            return mySpringSocialConfig;
        }
        return bean;
    }
}
