package com.sc.app.security.auth;

import com.sc.social.SocialAuthFilterPostProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.social.security.SocialAuthenticationFilter;
import org.springframework.stereotype.Component;

/**
 * @author sc
 * Created on  2018/1/24
 */
@Component
public class AppAuthFilterPostProcessor implements SocialAuthFilterPostProcessor {

    @Autowired
    private AuthenticationSuccessHandler appSuccessHandler;

    @Override
    public void process(SocialAuthenticationFilter socialAuthenticationFilter) {
        socialAuthenticationFilter.setAuthenticationSuccessHandler(appSuccessHandler);
    }
}
