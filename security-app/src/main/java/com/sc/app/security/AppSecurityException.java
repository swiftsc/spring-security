package com.sc.app.security;

/**
 * @author sc
 * Created on  2018/1/24
 */
public class AppSecurityException extends RuntimeException {
    public AppSecurityException(String msg){
        super(msg);
    }
}
