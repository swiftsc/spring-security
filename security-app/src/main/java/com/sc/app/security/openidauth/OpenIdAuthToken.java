package com.sc.app.security.openidauth;

import lombok.Data;
import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;

/**
 * @author sc
 * Created on  2018/1/24
 */
@Data
public class OpenIdAuthToken extends AbstractAuthenticationToken {

    private final Object principal;
    private String providerId;

    public OpenIdAuthToken(String openId,String providerId) {
        super(null);
        this.principal=openId;
        this.providerId = providerId;
        setAuthenticated(false);
    }

    public OpenIdAuthToken(Object userDetails, Collection<? extends GrantedAuthority> authorities){
        super(authorities);
        this.principal = userDetails;
        setAuthenticated(true);
    }


    @Override
    public Object getCredentials() {
        return null;
    }

    @Override
    public Object getPrincipal() {
        return null;
    }
}
