package com.sc.app.security;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;

/**
 * @author sc
 * Created on  2018/1/19
 * 认证服务器
 */
@Configuration
@EnableAuthorizationServer
public class AuthConfigServer {

}
