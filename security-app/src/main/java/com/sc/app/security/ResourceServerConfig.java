package com.sc.app.security;

import com.sc.app.security.openidauth.OpenIdAuthSecurityConfig;
import com.sc.security.config.ValidateCodeSecurityConfig;
import com.sc.security.mobile.SmsCodeAuthSecurityConfig;
import com.sc.security.properties.SecurityConstants;
import com.sc.security.properties.SecurityProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.social.security.SpringSocialConfigurer;

/**
 * @author sc
 * Created on  2018/1/19
 *
 * app环境安全配置
 */
@Configuration
@EnableResourceServer
public class ResourceServerConfig extends ResourceServerConfigurerAdapter{
    @Autowired
    private AuthenticationSuccessHandler appSuccessHandler;
    @Autowired
    private AuthenticationFailureHandler appFailedHandler;
    @Autowired
    private SpringSocialConfigurer mySocialSecurityConfig;
    @Autowired
    private SmsCodeAuthSecurityConfig smsCodeAuthSecurityConfig;
    @Autowired
    private SecurityProperties securityProperties;
    @Autowired
    private ValidateCodeSecurityConfig validateCodeSecurityConfig;
    @Autowired
    private OpenIdAuthSecurityConfig openIdAuthSecurityConfig;
    @Override
    public void configure(HttpSecurity http) throws Exception {
        http.formLogin()
                .loginPage(SecurityConstants.DEFAULT_UNAUTHENTICATION_URL)
                .loginProcessingUrl(SecurityConstants.DEFAULT_LOGIN_PROCESSING_URL_FORM)
                .successHandler(appSuccessHandler)
                .failureHandler(appFailedHandler);

        http.apply(validateCodeSecurityConfig)
                .and()
                .apply(smsCodeAuthSecurityConfig)
                .and()
                .apply(mySocialSecurityConfig)
                .and()
                .apply(openIdAuthSecurityConfig)
                .and()
                .authorizeRequests()
                .antMatchers(SecurityConstants.DEFAULT_UNAUTHENTICATION_URL,
                        securityProperties.getBrowser().getLoginPage(),
                        SecurityConstants.DEFAULT_LOGIN_PROCESSING_URL_MOBILE,
                        SecurityConstants.DEFAULT_LOGIN_PROCESSING_URL_FORM,
                        SecurityConstants.DEFAULT_VALIDATE_CODE_URL_PREFIX+"/*",
                        securityProperties.getBrowser().getSignUpUrl(),
                        "/user/register","/session/invalid")
                .permitAll()
                .anyRequest()
                .authenticated()
                .and()
                .csrf().disable();
    }
}
