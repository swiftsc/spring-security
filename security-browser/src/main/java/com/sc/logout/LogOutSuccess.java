package com.sc.logout;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sc.security.support.MyResponse;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author sc
 * Created on  2018/1/19
 */
@Slf4j
public class LogOutSuccess implements LogoutSuccessHandler {
    private String logOutSuccessUrl;
    private ObjectMapper objectMapper = new ObjectMapper();

    public LogOutSuccess(String logOutSuccessUrl) {
        this.logOutSuccessUrl = logOutSuccessUrl;
    }

    @Override
    public void onLogoutSuccess(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Authentication authentication) throws IOException, ServletException {
        log.info("用户"+authentication.getName()+"退出成功");
        if(StringUtils.isBlank(logOutSuccessUrl)){
                httpServletResponse.setContentType("application/json;charset=utf-8");
                httpServletResponse.getWriter().write(objectMapper.writeValueAsString(new MyResponse("退出成功")));
        }else{
            httpServletResponse.sendRedirect(logOutSuccessUrl);
        }
    }
}
