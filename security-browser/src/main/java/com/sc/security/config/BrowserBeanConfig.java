package com.sc.security.config;

import com.sc.logout.LogOutSuccess;
import com.sc.security.properties.SecurityProperties;
import com.sc.session.ExpiredStrategy;
import com.sc.session.InvalidStrategy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;
import org.springframework.security.web.session.InvalidSessionStrategy;
import org.springframework.security.web.session.SessionInformationExpiredStrategy;

/**
 * @author sc
 * Created on  2018/1/19
 */
@Configuration
public class BrowserBeanConfig {
    @Autowired
    private SecurityProperties securityProperties;

    @Bean
    @ConditionalOnMissingBean(InvalidSessionStrategy.class)
    public InvalidSessionStrategy invalidSessionStrategy(){
        return new InvalidStrategy(securityProperties.getSession().getSessionUrl());
    }

    @Bean
    @ConditionalOnMissingBean(SessionInformationExpiredStrategy.class)
    public SessionInformationExpiredStrategy sessionInformationExpiredStrategy(){
        return new ExpiredStrategy(securityProperties.getSession().getSessionUrl());
    }

    @Bean
    @ConditionalOnMissingBean(LogoutSuccessHandler.class)
    public LogoutSuccessHandler logoutSuccessHandler(){
        return new LogOutSuccess(securityProperties.getBrowser().getLogOutSuccessUrl());
    }
}
