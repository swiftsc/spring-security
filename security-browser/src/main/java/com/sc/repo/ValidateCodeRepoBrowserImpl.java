package com.sc.repo;

import com.sc.security.service.ValidateCodeType;
import com.sc.security.service.sms.ValidateCodeRepo;
import com.sc.security.validate.code.sms.ValidateCode;
import org.springframework.social.connect.web.HttpSessionSessionStrategy;
import org.springframework.social.connect.web.SessionStrategy;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.ServletWebRequest;

/**
 * @author sc
 * Created on  2018/1/24
 */
@Component
public class ValidateCodeRepoBrowserImpl implements ValidateCodeRepo {

    String SESSION_KEY = "SESSION_KEY_CODE_";
    private SessionStrategy sessionStrategy = new HttpSessionSessionStrategy();

    @Override
    public void save(ServletWebRequest request, ValidateCode validate, ValidateCodeType validateCodeType) {
        sessionStrategy.setAttribute(request,getSessionKey(validateCodeType),validate);
    }

    @Override
    public ValidateCode get(ServletWebRequest request, ValidateCodeType codeType) {

        return (ValidateCode) sessionStrategy.getAttribute(request,getSessionKey(codeType));
    }

    @Override
    public void remove(ServletWebRequest request, ValidateCodeType codeType) {
        sessionStrategy.removeAttribute(request,getSessionKey(codeType));
    }

    private String getSessionKey(ValidateCodeType validateCodeType){
        return SESSION_KEY+validateCodeType.toString().toUpperCase();
    }
}
