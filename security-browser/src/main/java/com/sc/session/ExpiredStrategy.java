package com.sc.session;

import org.springframework.security.web.session.SessionInformationExpiredEvent;
import org.springframework.security.web.session.SessionInformationExpiredStrategy;

import javax.servlet.ServletException;
import java.io.IOException;

/**
 * @author sc
 * Created on  2018/1/19
 */
public class ExpiredStrategy extends AbstractSessionStategy implements SessionInformationExpiredStrategy {
    public ExpiredStrategy(String invalidUrl) {
        super(invalidUrl);
    }

    @Override
    public void onExpiredSessionDetected(SessionInformationExpiredEvent event) throws IOException, ServletException {
        onSessionInvalid(event.getRequest(),event.getResponse());
    }

    @Override
    protected boolean isConcurrency() {
        return true;
    }
}
